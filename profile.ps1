Clear-Host
# My Powershell Profile:
Import-Module Terminal-Icons
Set-PSReadLineOption -PredictionSource History
Set-PSReadLineOption -PredictionViewStyle ListView

function flagColors() {
    $random = Get-Random 0, 1, 2
    if (0 -eq $random ) {
        # Flag of the greatest country on earth:
        Write-Host -NoNewline -BackgroundColor: Blue "* * * * * * * * * *"
        Write-Host -BackgroundColor: Red "                                "
        Write-Host -NoNewline -BackgroundColor: Blue "* * * * * * * * * *"
        Write-Host -BackgroundColor: White "                                "
        Write-Host -NoNewline -BackgroundColor: Blue "* * * * * * * * * *"
        Write-Host -BackgroundColor: Red "                                "
        Write-Host -NoNewLine -BackgroundColor: Blue "* * * * * * * * * *"
        Write-Host -BackgroundColor: White "                                "
        Write-Host -NoNewLine -BackgroundColor: Blue "* * * * * * * * * *"
        Write-Host -BackgroundColor: Red "                                "
        Write-Host -BackgroundColor: White "                                                   "
        Write-Host -BackgroundColor: Red "                                                   "
        Write-Host -BackgroundColor: White "                                                   "
        Write-Host -BackgroundColor: Red "                                                   "
        Write-Host -BackgroundColor: Black " "
    }

    elseif (1 -eq $random ) {
        # TODO: Make the flag bigger
        # German Flag
        Write-Host -BackgroundColor: Black "                                                        "
        Write-Host -BackgroundColor: Black "                                                        "
        Write-Host -BackgroundColor: Black "                                                        "
        Write-Host -BackgroundColor: Black "                                                        "
        Write-Host -BackgroundColor: Red "                                                        "
        Write-Host -BackgroundColor: Red "                                                        "
        Write-Host -BackgroundColor: Red "                                                        "
        Write-Host -BackgroundColor: Red "                                                        "
        Write-Host -BackgroundColor: Yellow "                                                        "
        Write-Host -BackgroundColor: Yellow "                                                        "
        Write-Host -BackgroundColor: Yellow "                                                        "
        Write-Host -BackgroundColor: Yellow "                                                        "
    }

    elseif (2 -eq $random) {
        #  French Flag
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
        Write-Host
        Write-Host -BackgroundColor: Blue -NoNewLine "                                  "
        Write-Host -BackgroundColor: White -NoNewLine  "                                  "
        Write-Host -BackgroundColor: Red -NoNewLine "                                  "
    }

    elseif (3 -eq $random) {
        # Random Colors
    }
}
flagColors

# Prompt:
function Prompt {
    "$pwd >"
}
<#
.SYNOPSIS
Reloads current powershell profile
#>
function Reload-Profile {
    @(
        $Profile.AllUsersAllHosts,
        $Profile.AllUsersCurrentHost,
        $Profile.CurrentUserAllHosts,
        $Profile.CurrentUserCurrentHost
    ) | % {
        if (Test-Path $_) {
            Write-Verbose "Running $_"
            . $_
        }
    }
}

function updateProfile() {
    try {
        cp profile.ps1 $profile
        print "Copy succeeded, please reload powershell"
        print "Reloading powershell"
        powershell
    }
    catch {
        print "Error: profile.ps1 not found"
    }
}

function randomInsult() {
    $insults = @(
        "You're an idiot",
        "Learn how to use the command line",
        "Try using the Get-Help command",
        "Learn how to type",
        "Nice one"
    )

    $color = @(
        "Red",
        "Blue",
        "Green",
        "White",
        "Yellow"
    )

    $result = Get-Random -Minimum 1 -Maximum $insults.Length
    $colorResult = Get-Random -Minimum 1 -Maximum $color.Length 
    Write-Host -ForegroundColor: $colorResult $insults[$result]
}

$ExecutionContext.InvokeCommand.CommandNotFoundAction = {
    Clear-Host
    randomInsult
    print "Command Not Found" -ForegroundColor:Red
}


# Aliases:
Set-Alias -Name "vscode" -Value "code"
Set-Alias -Name "touch" -Value "New-Item"
Set-Alias -Name "print" -Value "Write-Host"
Set-Alias -Name "clone" -Value "git clone"
Set-Alias -Name "add" -Value "git add"
Set-Alias -Name "pull" -Value "git pull"
Set-Alias -Name "push" -Value "git push"
Set-Alias -Name "commit" -Value "git commit"
Set-Alias -Name "branch" -Value "git branch"
Set-Alias -Name "remote" -Value "git remote"
Set-Alias -Name "debian" -Value "wsl -d Debian"
Set-Alias -Name "bash" -Value "wsl -d Debian"
Set-Alias -Name "ubuntu" -Value "wsl -d Ubuntu"
Set-Alias -Name "arch"  -Value "wsl -d Arch"
Set-Alias -Name "grep" -Value "findstr"